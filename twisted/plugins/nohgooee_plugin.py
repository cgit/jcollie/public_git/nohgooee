# -*- mode: python: coding: utf-8 -*-

# The contents of this file are subject to the BitTorrent Open Source License
# Version 1.1 (the License).  You may not copy or use this file, in either
# source code or executable form, except in compliance with the License.  You
# may obtain a copy of the License at http://www.bittorrent.com/license/.
#
# Software distributed under the License is distributed on an AS IS basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
# for the specific language governing rights and limitations under the
# License.

from NohGooee.platform import install_translation
install_translation()

from zope.interface import implements

from twisted.python import usage
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker
from twisted.application import internet
from twisted.web import server

from NohGooee.track import Tracker, InfoPage, Scrape, File, Announce, FavIcon
from NohGooee.config import NohgooeeOptions

class NohgooeeTrackerMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "nohgooee-tracker"
    description = "Nohgooee BitTorrent Tracker"
    options = NohgooeeOptions

    def makeService(self, options):
        
        tracker = Tracker(options)
        root = InfoPage(tracker)
        root.putChild('scrape', Scrape(tracker))
        root.putChild('file', File(tracker))
        root.putChild('announce', Announce(tracker))
        root.putChild('favicon.ico', FavIcon(tracker))

        site = server.Site(root)

        return internet.TCPServer(8080, site)

nohgooeeTrackerMaker = NohgooeeTrackerMaker()
