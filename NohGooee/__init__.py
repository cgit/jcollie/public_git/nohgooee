# -*- coding: utf-8 -*-
# The contents of this file are subject to the BitTorrent Open Source License
# Version 1.1 (the License).  You may not copy or use this file, in either
# source code or executable form, except in compliance with the License.  You
# may obtain a copy of the License at http://www.bittorrent.com/license/.
#
# Software distributed under the License is distributed on an AS IS basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
# for the specific language governing rights and limitations under the
# License.

app_name = 'NoGooee'
version = '1.0.0'

import sys
assert sys.version_info >= (2, 2, 1), _("Python %s or newer required") % '2.2.1'
import os
import time

branch = None
if os.access('.cdv', os.F_OK):
    branch = os.path.split(os.path.realpath(os.path.split(sys.argv[0])[0]))[1]

from NohGooee.language import languages, language_names
from NohGooee.platform import get_home_dir

if os.name == 'posix':
    if os.uname()[0] == "Darwin":
        from NohGooee.platform import install_translation
        install_translation()

del sys, get_home_dir

INFO = 0
WARNING = 1
ERROR = 2
CRITICAL = 3

status_dict = {INFO: 'info',
               WARNING: 'warning',
               ERROR: 'error',
               CRITICAL: 'critical'}

class BTFailure(Exception):
    pass
        
class BTShutdown(BTFailure):
    pass

