# -*- mode: python; coding: utf-8 -*-

# The contents of this file are subject to the BitTorrent Open Source License
# Version 1.1 (the License).  You may not copy or use this file, in either
# source code or executable form, except in compliance with the License.  You
# may obtain a copy of the License at http://www.bittorrent.com/license/.
#
# Software distributed under the License is distributed on an AS IS basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
# for the specific language governing rights and limitations under the
# License.

from lxml import etree
from twisted.python import usage

class NohgooeeOptions(usage.Options):
    
    optFlags = [
        ['allowed_controls',
         None, 
         _("allow special keys in torrents in the allowed_dir to affect "
           "tracker access")],


        ['allow_get', None,
         _("use with allowed_dir; adds a /file?hash={hash} url that "
           "allows users to download the torrent file"),
         ],

        ['close_with_rst', None,
         _("close connections with RST and avoid the TCP TIME_WAIT state")],

        ['keep_dead', None,
         _("keep dead torrents after they expire (so they still show up on your "
           "/scrape and web page). Only matters if allowed_dir is not set")],

        ['log_nat_checks', None,
         _("whether to add entries to the log for nat-check results")],

        ]

    optParameters = [
     
        ['allowed_dir',
         None,
         '',
         _("only allow downloads for .torrents in this dir (and recursively in "
           "subdirectories of directories that have no .torrent files "
           "themselves). If set, torrents in this directory show up on "
           "infopage/scrape whether they have peers or not")],

        ['bind', None,
         '',
         _("ip to bind to locally")],

        ['dfile', None,
         '/tmp/dfile.txt',
         _("file to store recent downloader info in")],

        ['favicon', None,
         '',
         _("file containing x-icon data to return when browser requests "
           "favicon.ico")],

        ['infopage_redirect', None,
         '',
         _("a URL to redirect the info page to")],
        
        ['max_give', None,
         200,
         _("maximum number of peers to give with any one request")],

        ['min_time_between_cache_refreshes', None,
         600.0,
         _("minimum time in seconds before a cache is considered stale "
           "and is flushed")],

        ['min_time_between_log_flushes', None,
         3.0,
         _("minimum time it must have been since the last flush to do "
           "another one")],

        ['nat_check', None,
         3,
         _("how many times to check if a downloader is behind a NAT "
           "(0 = don't check)")],

        ['only_local_override_ip', None,
         2,
         _("ignore the ip GET parameter from machines which aren't on "
           "local network IPs (0 = never, 1 = always, 2 = ignore if NAT "
           "checking is not enabled). HTTP proxy headers giving address "
           "of original client are treated the same as --ip.")],

        ['parse_dir_interval', None,
         60,
         _("how often to rescan the torrent directory, in seconds")],

        ['port', None,
         6969,
         _("Port to listen on.")],

        ['reannounce_interval', None,
         30 * 60,
         _("seconds downloaders should wait between reannouncements")],

        ['response_size', None,
         50,
         _("default number of peers to send an info message to if the "
           "client does not specify a number")],

        ['save_dfile_interval', None,
         5 * 60,
         _("seconds between saving dfile")],

        ['scrape_allowed', None,
         'full',
         _("scrape access allowed (can be none, specific or full)")],

        ['show_infopage', None, 
         True,
         _("whether to display an info page when the tracker's root dir "
           "is loaded")],

        ['show_names', None, 
         True,
         _("whether to display names from allowed dir")],

        ['socket_timeout', None,
         15,
         _("timeout for closing connections")],

        ['timeout_check_interval', None,
         5,
         _("time to wait between checking if any connections have timed out")],

        ['timeout_downloaders_interval', None,
         45 * 60,
         _("seconds between expiring downloaders")]
        ]

    
    

    

    
    
